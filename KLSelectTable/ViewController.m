//
//  ViewController.m
//  KLSelectTable
//
//  Created by klook on 2017/12/19.
//  Copyright © 2017年 klook. All rights reserved.
//

#import "ViewController.h"
#import "KLSelectTable.h"
#import "KLSelectTableRow.h"

@interface ViewController ()

@property (nonatomic, strong) KLSelectTable *selectTable;

@property (nonatomic, strong) NSMutableArray<KLSelectTableRow *> *rows;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
   
}

- (void)viewDidAppear:(BOOL)animated {
    self.selectTable = [KLSelectTable new];
    //    self.selectTable.config.isSectionSelectAllEnabled = YES;
    self.selectTable.rows = self.rows;
    [self.view addSubview:self.selectTable];
    NSLog(@"%@", NSStringFromUIEdgeInsets(self.view.safeAreaInsets));
    [self.selectTable setFrame:CGRectMake(0, self.view.safeAreaInsets.top, self.view.bounds.size.width, self.view.bounds.size.height-self.view.safeAreaInsets.top)];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSMutableArray<KLSelectTableRow *> *)rows
{
    if (!_rows) {
        _rows = [NSMutableArray array];
        KLSelectTableRow *row1 = [KLSelectTableRow new];
        row1.imageName = @"ico_activity_car";
        row1.title = @"Row1 title";
        
        KLSelectTableRow *row2 = [KLSelectTableRow new];
        row2.title = @"Row2 title";
        
        KLSelectTableRow *row3 = [KLSelectTableRow new];
        row3.title = @"Row3 title Row3 title Row3 title Row3 title Row3 title Row3 title Row3 title Row3 title";
        
        KLSelectTableRow *row4 = [KLSelectTableRow new];
        row4.title = @"Row4 title";
        row4.subTitle = @"Row4 subTitle";
        
        KLSelectTableRow *row5 = [KLSelectTableRow new];
        row5.title = @"Row5 title";
        row5.subTitle = @"Row5 subTitle";
        row5.isSelected = YES;
        
        KLSelectTableRow * row10 = [KLSelectTableRow new];
        row10.imageName = @"ico_activity_car";
        row10.title = @"Row10 title";
        
        KLSelectTableRow * row11 = [KLSelectTableRow new];
        row11.title = @"Row11 title";
        KLSelectTableRow * row12 = [KLSelectTableRow new];
        row12.title = @"Row12 title";
        row12.disable = YES;
        KLSelectTableRow * row13 = [KLSelectTableRow new];
        row13.title = @"Row13 title";
        row10.rows = @[row11,row12,row13];
        
        KLSelectTableRow * row20 = [KLSelectTableRow new];
        row20.imageName = @"ico_activity_car";
        row20.title = @"Row20 title";
        
        KLSelectTableRow * row21 = [KLSelectTableRow new];
        row21.title = @"Row21 title";
        KLSelectTableRow * row22 = [KLSelectTableRow new];
        row22.title = @"Row22 title";
        KLSelectTableRow * row23 = [KLSelectTableRow new];
        row23.title = @"Row23 title";
        row20.rows = @[row21,row22,row23];
        
        KLSelectTableRow * row300 = [KLSelectTableRow new];
        row300.imageName = @"ico_activity_car";
        row300.title = @"Row300 title";
        
        KLSelectTableRow * row310 = [KLSelectTableRow new];
        row310.title = @"row310 title";
        KLSelectTableRow * row311 = [KLSelectTableRow new];
        row311.title = @"row311 title";
        KLSelectTableRow * row312 = [KLSelectTableRow new];
        row312.title = @"row312 title";
        KLSelectTableRow * row313 = [KLSelectTableRow new];
        row313.title = @"row313 title";
        row310.rows = @[row311,row312,row313];
        
        KLSelectTableRow * row320 = [KLSelectTableRow new];
        row320.title = @"row320 title";
        KLSelectTableRow * row330 = [KLSelectTableRow new];
        row330.title = @"row330 title";
        
        
        row300.rows = @[row310,row320,row330];
        
    
        [_rows addObject:row1];
        [_rows addObject:row2];
        [_rows addObject:row3];
        [_rows addObject:row4];
        [_rows addObject:row5];
        [_rows addObject:row10];
        [_rows addObject:row20];
        [_rows addObject:row300];
    }
    return _rows;
}


@end
