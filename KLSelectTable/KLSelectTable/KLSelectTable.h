//
//  KLSelectTable.h
//  KLSelectTable
//
//  Created by klook on 2017/12/19.
//  Copyright © 2017年 klook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KLSelectTableConfig.h"
#import "KLSelectTableRow.h"

@class KLSelectTable;
@protocol KLSelectTableDelegate<NSObject>
@optional
- (void)selectTable:(KLSelectTable *)selectTable didSelectTableRow:(KLSelectTableRow *)selectTableRow;

@end

@interface KLSelectTable : UIView

@property (nonatomic, strong) KLSelectTableConfig *config; // 配置

@property (nonatomic, strong) NSArray<KLSelectTableRow *> *rows; // 数据源
@property (nonatomic, weak  ) id<KLSelectTableDelegate> delegate;
@property (nonatomic, strong, readonly) UITableView *tableView;


- (void)resetSelected;

- (NSDictionary *)selectedResult; // 选择的结果字典

/// 获取选中的rows。如果某个row的childRows全选，则只添加该row
- (NSArray<KLSelectTableRow *> *)selectedTopmostRows;

- (BOOL)hasSelected;

- (void)setTableViewBackGroundColor:(UIColor *)color;


@end
