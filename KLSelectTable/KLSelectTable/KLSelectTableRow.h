//
//  KLSelectTableRow.h
//  KLSelectTable
//
//  Created by klook on 2017/12/19.
//  Copyright © 2017年 klook. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface KLSelectTableRow : NSObject <NSCopying>

@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *subTitle;

@property (nonatomic, strong) NSString *value;

@property (nonatomic, weak) KLSelectTableRow *parentRow;

@property (nonatomic, strong) NSArray<KLSelectTableRow *> *rows;

@property (nonatomic, assign) BOOL isSelected;  // 是否选择，menu 是否全选
@property (nonatomic, assign) BOOL isUnfold;    // 是否展开
@property (nonatomic, assign) BOOL hideAssistView;  // 隐藏辅助视图
@property (nonatomic, assign) BOOL disable;


@property (nonatomic, strong) NSIndexPath *indexPath;

- (void)setRows:(NSArray<KLSelectTableRow *> *)rows;

- (BOOL)isMenu;

- (BOOL)isAllSelected;

- (BOOL)hasSelected;

- (BOOL)hasDisableChildRow;

- (NSInteger)getRowLevel;

- (NSString *)rowValue;

- (NSArray<KLSelectTableRow *> *)getChildRows;

- (NSArray<KLSelectTableRow *> *)getAllChildRows;

- (NSArray<KLSelectTableRow *> *)getParentRows;


/**
 获取row的所有选中的叶子节点的rowValue数组，返回的结果字典的key为该row的rowValue，value为所有选中的叶子节点row的rowValue值数组
 如果该row为叶子节点row且被选中，则返回的结果字典的key为该row的rowValue，value均为该row的rowValue值数组

 @return 结果字典，key为为该row的rowValue，value为所有选中的叶子节点row的rowValue值数组，如果自身也是叶子节点，则只包含自身。
 */
- (NSDictionary *)getAllSelectedLeafRowValues;

/// 获取最上层的选中的ChildRows，包含自身
- (NSArray<KLSelectTableRow *> *)getSelectedTopmostRows;

- (NSArray<KLSelectTableRow *> *)clicked;

@end

