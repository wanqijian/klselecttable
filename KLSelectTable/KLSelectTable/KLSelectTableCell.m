//
//  KLSelectTableCell.m
//  KLSelectTable
//
//  Created by klook on 2017/12/19.
//  Copyright © 2017年 klook. All rights reserved.
//

#import "KLSelectTableCell.h"

@interface KLSelectTableCell ()

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UIView *assistView;
@property (nonatomic, strong) UIImageView *assistImageView;
@property (nonatomic, strong) UILabel *titleLabel;
//@property (nonatomic, strong) UILabel *subtitleLabel;

@property (nonatomic, strong) UIView *separatorLine;

@property (nonatomic, assign) CGFloat screenWidth;
@property (nonatomic, assign) CGFloat marginLeft;


@end

@implementation KLSelectTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self installViews];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    

    
}


- (void)installViews
{
    if (![self.subviews containsObject:self.iconImageView]) {
        self.iconImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.iconImageView];
    }
    if (![self.subviews containsObject:self.assistView]) {
        self.assistView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.assistView];
    }
    if (![self.assistView.subviews containsObject:self.assistImageView]) {
        self.assistImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.assistView addSubview:self.assistImageView];
    }
    if (![self.subviews containsObject:self.titleLabel]) {
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.titleLabel];
    }
//    if (![self.subviews containsObject:self.subtitleLabel]) {
//        self.subtitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
//        [self addSubview:self.subtitleLabel];
//    }
    
    if (![self.subviews containsObject:self.separatorLine]) {
        self.separatorLine.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.separatorLine];
    }
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAssistView)];
    self.assistView.userInteractionEnabled = YES;
    [self.assistView addGestureRecognizer:tapGR];
    
}

- (void)updateViews
{
    [self removeConstraints:self.constraints];
    
    self.titleLabel.font = [UIFont systemFontOfSize:16];
    if (self.row.parentRow) {
        self.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    
    CGFloat contentLeft = self.marginLeft;
    [self refreshImage];
    
    {
        NSInteger marginTop = 12;
        if (self.row.parentRow) {
            marginTop = 8;
        }
        
        // 图片最右边，垂直居中，大小24x24
        NSLayoutConstraint *viewLeft = [NSLayoutConstraint constraintWithItem:self.iconImageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:self.marginLeft];
        NSLayoutConstraint *viewCentreY = [NSLayoutConstraint constraintWithItem:self.iconImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
        NSLayoutConstraint *viewWidth = [NSLayoutConstraint constraintWithItem:self.iconImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:24];
        NSLayoutConstraint *viewHeight = [NSLayoutConstraint constraintWithItem:self.iconImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:24];
        NSLayoutConstraint *viewTop = [NSLayoutConstraint constraintWithItem:self.iconImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:marginTop];
        
        [self addConstraint:viewWidth];
        [self addConstraint:viewHeight];
        [self addConstraint:viewTop];
        [self addConstraint:viewCentreY];
        [self addConstraint:viewLeft];
        
        contentLeft += 24+16;
    }
    
    {
        NSInteger assistViewWidth = 64;
        NSInteger assistViewHeight = 48;
        if (self.row.parentRow) {
            assistViewHeight = 40;
        }
        CGFloat left = self.screenWidth-8-48;
        NSLayoutConstraint *viewLeft = [NSLayoutConstraint constraintWithItem:self.assistView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:left];
        
        NSLayoutConstraint *viewCentreY = [NSLayoutConstraint constraintWithItem:self.assistView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
        NSLayoutConstraint *viewWidth = [NSLayoutConstraint constraintWithItem:self.assistView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:assistViewWidth];
        NSLayoutConstraint *viewHeight = [NSLayoutConstraint constraintWithItem:self.assistView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:assistViewHeight];
        NSLayoutConstraint *viewTop = [NSLayoutConstraint constraintWithItem:self.assistView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        
        [self addConstraint:viewCentreY];
        [self addConstraint:viewLeft];
        [self addConstraint:viewWidth];
        [self addConstraint:viewHeight];
        [self addConstraint:viewTop];
        
        {
            NSLayoutConstraint *viewCenterY = [NSLayoutConstraint constraintWithItem:self.assistImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.assistView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
            NSLayoutConstraint *viewRight = [NSLayoutConstraint constraintWithItem:self.assistImageView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.assistView attribute:NSLayoutAttributeRight multiplier:1 constant:-16];
            NSLayoutConstraint *viewWidth = [NSLayoutConstraint constraintWithItem:self.assistImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:24];
            NSLayoutConstraint *viewHeight = [NSLayoutConstraint constraintWithItem:self.assistImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:24];
            
            [self addConstraint:viewCenterY];
            [self addConstraint:viewRight];
            [self addConstraint:viewWidth];
            [self addConstraint:viewHeight];
            
        }
        if (self.row.hideAssistView) {
            self.assistView.hidden = YES;
        } else {
            self.assistView.hidden = NO;
        }
        
    }
    
    {
        CGFloat titleLabelHeight = 16;
        CGFloat maxWidth = self.screenWidth-contentLeft-48-12;
        if (self.row.title.length && self.row.subTitle.length) {
            NSString *titleString = [NSString stringWithFormat:@"%@\n%@", self.row.title, self.row.subTitle];
            NSRange titleRange = [titleString rangeOfString:self.row.title];
            NSRange subTitleRange = [titleString rangeOfString:[NSString stringWithFormat:@"\n%@", self.row.subTitle]];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titleString];
            [attributedString addAttribute:NSFontAttributeName value:self.titleLabel.font range:titleRange];
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithWhite:0 alpha:0.87] range:titleRange];
            [attributedString addAttribute:NSFontAttributeName value:self.titleLabel.font range:subTitleRange];
            [attributedString addAttribute:NSForegroundColorAttributeName value: [UIColor colorWithWhite:0 alpha:0.38] range:subTitleRange];
            self.titleLabel.attributedText = attributedString;
            CGRect bounds = [attributedString boundingRectWithSize:CGSizeMake(maxWidth, CGFLOAT_MAX)
                                                               options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                                               context:nil];
            titleLabelHeight = ceil(bounds.size.height);
        } else if (!self.row.subTitle || self.row.subTitle.length == 0) {
            self.titleLabel.text = self.row.title;
            titleLabelHeight = [self sizeWithString:self.row.title font:self.titleLabel.font maxSize:CGSizeMake(maxWidth, CGFLOAT_MAX)].height;
        }
        
        NSLayoutConstraint *viewLeft = [NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.iconImageView attribute:NSLayoutAttributeRight multiplier:1 constant:16];
        [self addConstraint:viewLeft];
        NSLayoutConstraint *viewWidth = [NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:maxWidth];
        [self addConstraint:viewWidth];
        NSLayoutConstraint *viewHeight = [NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:titleLabelHeight];
        [self addConstraint:viewHeight];
        NSLayoutConstraint *viewTop = [NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:10];
        viewTop.priority = UILayoutPriorityDefaultHigh;
        [self addConstraint:viewTop];
        NSLayoutConstraint *viewCentY = [NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.iconImageView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
        [self addConstraint:viewCentY];
//        NSLayoutConstraint *viewBottom = [NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationLessThanOrEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:-12];
//        viewBottom.priority = UILayoutPriorityDefaultHigh;
        [self addConstraint:viewCentY];
    }
    
    {
        NSLayoutConstraint *lineLeft = [NSLayoutConstraint constraintWithItem:self.separatorLine attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:self.marginLeft];
        NSLayoutConstraint *lineBottom = [NSLayoutConstraint constraintWithItem:self.separatorLine attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        NSLayoutConstraint *lineRight = [NSLayoutConstraint constraintWithItem:self.separatorLine attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0];
        NSLayoutConstraint *lineHeight = [NSLayoutConstraint constraintWithItem:self.separatorLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:(1/[UIScreen mainScreen].scale)];
        
        [self addConstraint:lineLeft];
        [self addConstraint:lineBottom];
        [self addConstraint:lineRight];
        [self addConstraint:lineHeight];
    }
}

- (void)setConstraintWithContentView:(UIView *)view left:(CGFloat)left width:(CGFloat)width height:(CGFloat)height
{
    NSLayoutConstraint *viewLeft = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:left];
    NSLayoutConstraint *viewWidth = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:width];
    NSLayoutConstraint *viewHeight = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:ceil(height)];
    
    [self addConstraint:viewLeft];
    [self addConstraint:viewWidth];
    [self addConstraint:viewHeight];
}

#pragma mark - Function

- (void)refreshImage
{
    if ([self.row isMenu]) {
        // Menu
        if (self.row.disable) {
            self.iconImageView.image = [UIImage imageNamed:self.config.disSelectImageName];
            self.assistImageView.image = nil;
        } else {
            if (self.row.isSelected) {
                self.iconImageView.image = [UIImage imageNamed:self.config.selectedImageName];
            } else if ([self.row hasSelected])  {
                self.iconImageView.image = [UIImage imageNamed:self.config.halfSelectImageName];
            } else {
                self.iconImageView.image = [UIImage imageNamed:self.config.unselectedImageName];
            }
            if (self.row.isUnfold) {
                self.assistImageView.image = [UIImage imageNamed:self.config.unfoldImageName];
            } else {
                self.assistImageView.image = [UIImage imageNamed:self.config.foldImageName];
            }
        }
        
    } else {
        // Item
        if (self.row.disable) {
            self.iconImageView.image = [UIImage imageNamed:self.config.disSelectImageName];
        } else {
            if (self.row.isSelected) {
                self.iconImageView.image = [UIImage imageNamed:self.config.selectedImageName];
            } else {
                self.iconImageView.image = [UIImage imageNamed:self.config.unselectedImageName];
            }
        }
        
        self.assistImageView.image = nil;
    }
}

- (void)tapAssistView
{
    NSLog(@"tapAssistImageView");
    if (self.tapAssistViewBlock && !self.row.disable) {
        self.tapAssistViewBlock(self.row);
    }
}


#pragma mark - setter/getter

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [UIImageView new];
        _iconImageView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _iconImageView;
}

- (UIView *)assistView
{
    if (!_assistView) {
        _assistView = [UIView new];
        _assistView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _assistView;
}

- (UIImageView *)assistImageView
{
    if (!_assistImageView) {
        _assistImageView = [UIImageView new];
        _assistImageView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _assistImageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.numberOfLines = 0;
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
//        _titleLabel.font = [UIFont systemFontOfSize:16];
    }
    return _titleLabel;
}

- (UIView *)separatorLine
{
    if (!_separatorLine) {
        _separatorLine = [UIView new];
        _separatorLine.backgroundColor = [UIColor colorWithWhite:0 alpha:0.12];
        _separatorLine.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _separatorLine;
}

- (void)setRow:(KLSelectTableRow *)row
{
    _row = row;
    _isSelected = row.isSelected;
    if (row.disable) {
        _titleLabel.textColor = [UIColor colorWithWhite:0 alpha:0.38];
    } else {
        _titleLabel.textColor = [UIColor colorWithWhite:0 alpha:0.87];
    }
    
    [self updateViews];
}

- (void)setConfig:(KLSelectTableConfig *)config
{
    _config = config;
    
}

- (void)setWithRow:(KLSelectTableRow *)row config:(KLSelectTableConfig *)config
{
    self.config = config;
    self.row = row;
    
}

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    if (self.row.isSelected != isSelected) {
        self.row.isSelected = isSelected;
    }
    
    [self refreshImage];
}

- (CGFloat)screenWidth
{
    return [UIScreen mainScreen].bounds.size.width;
}

- (CGFloat)marginLeft
{
    _marginLeft = [self.row getRowLevel] * 16;
    return _marginLeft;
}

#pragma mark - Util

- (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    [attrs setValue:font forKey:NSFontAttributeName];
    
    CGSize size = [string boundingRectWithSize:maxSize
                                options: NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                             attributes:attrs
                                context:nil].size;
    return CGSizeMake(ceil(size.width), ceil(size.height));
}


@end

