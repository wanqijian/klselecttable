//
//  KLSelectTableConfig.m
//  KLSelectTable
//
//  Created by klook on 2017/12/19.
//  Copyright © 2017年 klook. All rights reserved.
//

#import "KLSelectTableConfig.h"

@implementation KLSelectTableConfig

+ (KLSelectTableConfig *)defaultConfig
{
    static KLSelectTableConfig *_defaultConfig = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _defaultConfig = [[self alloc] init];
        
        _defaultConfig.allTitle = @"All";
        _defaultConfig.unfoldImageName = @"iconSelectTableUnfold.png";
        _defaultConfig.foldImageName = @"iconSelectTableFold.png";
        _defaultConfig.unselectedImageName = @"iconSelectTableUnselected.png";
        _defaultConfig.selectedImageName = @"iconSelectTableSelected.png";
        _defaultConfig.disSelectImageName = @"iconSelectTableDisSelect.png";
        _defaultConfig.halfSelectImageName = @"iconSelectTablekHalfSelected.png";
        _defaultConfig.showFilter = YES;
        
//        _defaultConfig.iconMarginTop = 12;
    });
    

    return _defaultConfig;
}


@end
