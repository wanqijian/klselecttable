//
//  KLSelectTableConfig.h
//  KLSelectTable
//
//  Created by klook on 2017/12/19.
//  Copyright © 2017年 klook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KLSelectTableConfig : NSObject

@property (nonatomic, assign) BOOL isSectionSelectAllEnabled; // section是否支持选择全部
@property (nonatomic, assign) BOOL showItemSeparatorLine;
@property (nonatomic, strong) NSString *allTitle; // 选择所有的文案
@property (nonatomic, strong) NSString *unfoldImageName; // 展开图片名称
@property (nonatomic, strong) NSString *foldImageName; // 折叠图片名称
@property (nonatomic, strong) NSString *selectedImageName; // 选中图片名称
@property (nonatomic, strong) NSString *unselectedImageName; // 没选中图片名称
@property (nonatomic, strong) NSString *disSelectImageName; // 不可以选择图片名称
@property (nonatomic, strong) NSString *halfSelectImageName; // 半选择图片名称


@property (nonatomic, assign) BOOL showFilter;


//@property (nonatomic, assign) CGFloat iconMarginTop;
//@property (nonatomic, assign) CGFloat iconMarginLeft;
//@property (nonatomic, assign) CGFloat iconMarginBottom;
//@property (nonatomic, assign) CGFloat iconSizeWidth;
//@property (nonatomic, assign) CGFloat iconSizeHeight;




+ (KLSelectTableConfig *)defaultConfig;

@end
