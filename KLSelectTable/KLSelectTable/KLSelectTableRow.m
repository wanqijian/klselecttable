//
//  KLSelectTableRow.m
//  KLSelectTable
//
//  Created by klook on 2017/12/19.
//  Copyright © 2017年 klook. All rights reserved.
//

#import "KLSelectTableRow.h"

@interface KLSelectTableRow()

@end

@implementation KLSelectTableRow

- (void)setRows:(NSArray<KLSelectTableRow *> *)rows
{
    _rows = rows;
    for (KLSelectTableRow *row in rows) {
        [row setParent:self];
    }
}

- (void)setParent:(KLSelectTableRow *)parentRow
{
    _parentRow = parentRow;
}

- (BOOL)isMenu
{
    if (self.rows && self.rows.count) {
        return YES;
    }
    return NO;
}

- (BOOL)isMenuDisable
{
    BOOL disable = YES;
    if ([self isMenu]) {
        for (KLSelectTableRow *row in self.rows) {
            if (!row.disable) {
                disable = NO;
                break;
            }
        }
    }
    return disable;
}


/**
 针对menu调用
 如果是叶子节点，则直接返回NO
 如果是disable节点，则直接返回NO

 @return 子节点是否全部选中
 */
- (BOOL)isAllSelected {
    BOOL isAllSelected = YES;
    if (!self.disable && [self isMenu]) {
        for (KLSelectTableRow *row in self.rows) {
            if (!row.disable && [row isMenu]) {
                isAllSelected = [row isAllSelected];
                if (!isAllSelected) {
                    break;
                }
            } else if (!row.disable && !row.isSelected) {
                isAllSelected = NO;
                break;
            } else if (row.disable) {
                isAllSelected = NO;
                break;
            }
        }
    } else {
        isAllSelected = NO;
    }
    
    return isAllSelected;
}

- (BOOL)hasSelected {
    BOOL hasSelected = NO;
    if (!self.disable && [self isMenu]) {
        for (KLSelectTableRow *row in self.rows) {
            if (!row.disable && [row isMenu]) {
                hasSelected = [row hasSelected];
                if (hasSelected) {
                    break;
                }
            } else if (!row.disable && row.isSelected) {
                hasSelected = YES;
                break;
            }
        }
    } else {
        hasSelected = NO;
    }
    return hasSelected;
}

- (BOOL)hasDisableChildRow {
    BOOL hasDisable = NO;
    for (KLSelectTableRow *row in [self getAllChildRows]) {
        if (row.disable) {
            hasDisable = YES;
            break;
        }
    }
    return hasDisable;
}

- (id)copyWithZone:(NSZone *)zone
{
    KLSelectTableRow *copy = [[[self class] allocWithZone:zone] init];
    copy.imageName = self.imageName;
    copy.title = self.title;
    copy.subTitle = self.subTitle;
    copy.value = self.value;
    copy.parentRow = self.parentRow;
    copy.rows = self.rows;
    copy.isSelected = self.isSelected;
    
    return copy;
}

- (NSInteger)getRowLevel
{
    
    NSInteger level = 1;
    if (self.parentRow) {
        level += [self.parentRow getRowLevel];
    }
    return level;
}

- (NSString *)rowValue
{
    if (self.value) {
        return self.value;
    } else if(self.title) {
        return self.title;
    }
    return @"";
}

- (NSArray<KLSelectTableRow *> *)getChildRows
{
    NSMutableArray *childRows = [NSMutableArray array];
    if (self.isUnfold) {
        for (KLSelectTableRow *row in self.rows) {
            [childRows addObject:row];
            if ([row isMenu]) {
                [childRows addObjectsFromArray:[row getChildRows]];
            }
        }
    }
    return childRows;
}



/**
 是否包含disable的row

 @return 是表示包含，否表示不包含
 */
- (BOOL)containDisabledChildRows {
    BOOL contains = NO;
    if (self.disable) {
        contains = YES;
    } else {
        for (KLSelectTableRow *row in self.rows) {
            contains = [row containDisabledChildRows];
            if (contains) {
                break;
            }
        }
    }
    return contains;
}

- (NSArray<KLSelectTableRow *> *)getSelectedTopmostRows {
    NSMutableArray *rows = [NSMutableArray array];
    if (self.isSelected && ![self containDisabledChildRows]) {
        [rows addObject:self];
    } else {
        for (KLSelectTableRow *row in self.rows) {
            [rows addObjectsFromArray:[row getSelectedTopmostRows]];
        }
    }
    
    return rows;
}

- (NSArray<KLSelectTableRow *> *)getAllChildRows
{
    NSMutableArray *childRows = [NSMutableArray array];
    for (KLSelectTableRow *row in self.rows) {
        [childRows addObject:row];
        if ([row isMenu]) {
            [childRows addObjectsFromArray:[row getAllChildRows]];
        }
    }
    return childRows;
}

- (NSArray<KLSelectTableRow *> *)getSelectionChangedChildRows
{
    NSMutableArray *selectionChangedChildRows = [NSMutableArray array];
    for (KLSelectTableRow *row in self.rows) {
        if (!row.disable) {
            if (row.isSelected != self.isSelected) {
                row.isSelected = self.isSelected;
                [selectionChangedChildRows addObject:row];
            }
            if ([row isMenu]) {
                [selectionChangedChildRows addObjectsFromArray:[row getSelectionChangedChildRows]];
            }
        }
    }
    if ([self hasDisableChildRow] && self.isSelected) {
        self.isSelected = NO;
    }
    return selectionChangedChildRows;
}

- (NSArray<KLSelectTableRow *> *)getParentRows {
    NSMutableArray *parentRows = [NSMutableArray array];
    if (self.parentRow) {
        if ([self.parentRow isAllSelected]) {
            self.parentRow.isSelected = YES;
        } else {
            self.parentRow.isSelected = NO;
        }
        [parentRows addObjectsFromArray:[self.parentRow getParentRows]];
        [parentRows addObject:self.parentRow];
    }
    return parentRows;
}

- (NSArray<KLSelectTableRow *> *)getSelectionChangedParentRows
{
    NSMutableArray *selectionChangedParentRows = [NSMutableArray array];
    if (self.parentRow) {
        if ([self.parentRow isAllSelected]) {
            if (!self.parentRow.isSelected) {
                [selectionChangedParentRows addObject:self.parentRow];
            }
            self.parentRow.isSelected = YES;
        } else if ([self.parentRow hasSelected]) {
            [selectionChangedParentRows addObject:self.parentRow];
            self.parentRow.isSelected = NO;
        } else {
            [selectionChangedParentRows addObject:self.parentRow];
            self.parentRow.isSelected = NO;
        }
        [selectionChangedParentRows addObjectsFromArray:[self.parentRow getSelectionChangedParentRows]];
    } else {
        [selectionChangedParentRows addObject:self];
    }
    
    return selectionChangedParentRows;
}

- (NSDictionary *)getAllSelectedLeafRowValues
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSMutableSet *valueSet = [NSMutableSet set];
    
    if (!self.disable && [self isMenu]) {
        for (KLSelectTableRow *row in self.rows) {
            [valueSet addObjectsFromArray:[[row getAllSelectedLeafRowValues].allValues firstObject]];
        }
        if (valueSet.allObjects.count) {
            [dict setValue:valueSet.allObjects forKey:self.rowValue];
        }
    } else if (!self.disable && self.isSelected) {
        [valueSet addObject:self.rowValue];
        [dict setValue:valueSet.allObjects forKey:self.rowValue];
    }
    return dict;
}

- (NSArray<KLSelectTableRow *> *)clicked {
    NSMutableArray *destRows = nil;
    if (!self.disable) {
        if ([self isMenu]) {
            if (self.isSelected) {
                self.isSelected = NO;
            } else if (![self hasSelected]) {
                self.isSelected = YES;
            }
            
        } else {
            self.isSelected = !self.isSelected;
        }
//        self.isSelected = !self.isSelected;
        NSMutableArray *reloadRows = [NSMutableArray array];
        [reloadRows addObject:self];
        if (self.isMenu) {
            NSArray *selectionChangedChildRows = [self getSelectionChangedChildRows];
            [reloadRows addObjectsFromArray:selectionChangedChildRows];
            
            if (self.parentRow) {
                NSArray *selectionChangedParentRows = [self getSelectionChangedParentRows];
                [reloadRows addObjectsFromArray:selectionChangedParentRows];
            }
        } else {
            if (self.parentRow) {
                NSArray *selectionChangedParentRows = [self getSelectionChangedParentRows];
                [reloadRows addObjectsFromArray:selectionChangedParentRows];
            }
        }
        destRows = reloadRows;
    }
    return destRows;
}

@end

