//
//  KLSelectTableCell.h
//  KLSelectTable
//
//  Created by klook on 2017/12/19.
//  Copyright © 2017年 klook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KLSelectTableRow.h"
#import "KLSelectTable.h"

@interface KLSelectTableCell : UITableViewCell

@property (nonatomic, strong) KLSelectTableRow *row;
@property (nonatomic, strong) KLSelectTableConfig *config;

@property (nonatomic, assign) BOOL isSelected;

@property (nonatomic, copy) void(^tapAssistViewBlock)(KLSelectTableRow *row);

- (void)setWithRow:(KLSelectTableRow *)row config:(KLSelectTableConfig *)config;

@end

