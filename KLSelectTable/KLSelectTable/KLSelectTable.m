//
//  KLSelectTable.m
//  KLSelectTable
//
//  Created by klook on 2017/12/19.
//  Copyright © 2017年 klook. All rights reserved.
//

#import "KLSelectTable.h"
#import "KLSelectTableCell.h"

@interface KLSelectTable () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIView *filterView;
@property (nonatomic, strong) UIView *filterBGView;
@property (nonatomic, strong) UIImageView *filterIcon;
@property (nonatomic, strong) UITextField *filterTextField;
@property (nonatomic, strong, readwrite) UITableView *tableView;
@property (nonatomic, strong) NSArray<KLSelectTableRow *> *dataSource;

@property (nonatomic, strong) NSMutableArray<KLSelectTableRow *> *tableDataSource;

@property (nonatomic, assign) BOOL isFilter;
@property (nonatomic, strong) NSMutableArray<KLSelectTableRow *> *filterDataSource;

@end

@implementation KLSelectTable

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    [self addSubview:self.filterView];
    [self addSubview:self.tableView];
    
    
    [self updateViews];
}

- (void)updateViews
{
    [self removeConstraints:self.constraints];
    
    CGFloat filterHeight = 0.f;
    if (self.config.showFilter) {
        filterHeight = 56.f;
    }
    
    NSLayoutConstraint *filterViewTop = [NSLayoutConstraint constraintWithItem:self.filterView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *filterViewLeft = [NSLayoutConstraint constraintWithItem:self.filterView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    NSLayoutConstraint *filterViewRight = [NSLayoutConstraint constraintWithItem:self.filterView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    NSLayoutConstraint *filterViewHeight = [NSLayoutConstraint constraintWithItem:self.filterView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:filterHeight];
    
    [self addConstraint:filterViewTop];
    [self addConstraint:filterViewLeft];
    [self addConstraint:filterViewRight];
    [self addConstraint:filterViewHeight];
    
    NSLayoutConstraint *tableViewTop = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.filterView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    NSLayoutConstraint *tableViewLeft = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    NSLayoutConstraint *tableViewBottom = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    NSLayoutConstraint *tableViewRight = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    
    [self addConstraint:tableViewTop];
    [self addConstraint:tableViewLeft];
    [self addConstraint:tableViewBottom];
    [self addConstraint:tableViewRight];
}

- (void)expandContractionWithRow:(KLSelectTableRow *)row indexPath:(NSIndexPath *)indexPath
{
    if (row.disable) {
        return;
    }
    // 不是菜单，没有这个功能
    if (!row.isMenu) {
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
        return;
    }
    NSInteger rowIndex = indexPath.row;
    // 判断现在是否展开还是收起
    if (!row.isUnfold) {
        // 展开
        row.isUnfold = !row.isUnfold;
        NSArray *insertRows = [row getChildRows];
        NSMutableArray *insertRowIndexPaths = [NSMutableArray array];
        NSIndexSet *insertIndexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(rowIndex+1, insertRows.count)];
        for (int i=0; i<insertRows.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(rowIndex+i+1) inSection:0];
            [insertRowIndexPaths addObject:indexPath];
        }
        
        [self.tableView beginUpdates];
        [self.tableDataSource insertObjects:insertRows atIndexes:insertIndexSet];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView insertRowsAtIndexPaths:insertRowIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        [self.tableView endUpdates];
    } else {
        // 收起
        NSArray *removeRows = [row getChildRows];
        NSMutableArray *removeRowIndexPaths = [NSMutableArray array];
        for (int i=0; i<removeRows.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(rowIndex+i+1) inSection:0];
            [removeRowIndexPaths addObject:indexPath];
        }
        row.isUnfold = !row.isUnfold;
        [self.tableView beginUpdates];
        [self.tableDataSource removeObjectsInArray:removeRows];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView deleteRowsAtIndexPaths:removeRowIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.filterTextField isEditing]) {
        [self.filterTextField endEditing:YES];
    }
}



#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableDataSource count];
}

#pragma mark - UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell ;
    
    KLSelectTableRow *selectTableRow = self.tableDataSource[indexPath.row];
    
    if (self.isFilter) {
        selectTableRow.hideAssistView = YES;
    } else {
        selectTableRow.hideAssistView = NO;
    }
    
    selectTableRow.indexPath = indexPath;
    KLSelectTableCell *selectTableCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([KLSelectTableCell class]) forIndexPath:indexPath];
    [selectTableCell setWithRow:selectTableRow config:self.config];
    
    __weak typeof(self) weakSelf = self;
    selectTableCell.tapAssistViewBlock = ^(KLSelectTableRow *row) {
        // 展开|收缩
        NSInteger rowIndex = [self.tableDataSource indexOfObject:row];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:0];
        [weakSelf expandContractionWithRow:row indexPath:indexPath];
    };
    
    if (selectTableRow.getRowLevel == 1) {
        selectTableCell.backgroundColor = [UIColor whiteColor];
    } else {
        selectTableCell.backgroundColor = [UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1];
    }
    
    cell = selectTableCell;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    KLSelectTableCell *selectTableCell = [tableView cellForRowAtIndexPath:indexPath];
    KLSelectTableRow *selectedRow = selectTableCell.row;

    NSMutableArray *reloadIndexPaths = [NSMutableArray array];
    NSArray *selectionChangedRows = [selectedRow clicked];
    if (selectionChangedRows) {
        for (KLSelectTableRow *row in selectionChangedRows) {
            NSUInteger index = [self.tableDataSource indexOfObject:row];
            if (index != NSNotFound) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                [reloadIndexPaths addObject:indexPath];
            }
        }
        
        [self.tableView reloadRowsAtIndexPaths:reloadIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(selectTable:didSelectTableRow:)]) {
            [self.delegate selectTable:self didSelectTableRow:selectedRow];
        }
    }
}

#pragma mark - getter/setter

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_tableView registerClass:[KLSelectTableCell class] forCellReuseIdentifier:NSStringFromClass([KLSelectTableCell class])];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = [UIScreen mainScreen].bounds.size.height;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
        }
        _tableView.estimatedRowHeight = 44;
        
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self.tableView setFrame:frame];
}


- (KLSelectTableConfig *)config
{
    if (!_config) {
        _config = [KLSelectTableConfig defaultConfig];
    }
    return _config;
}

- (void)setRows:(NSArray<KLSelectTableRow *> *)rows
{
    _rows = rows;
    self.dataSource = rows;
    
    if (self.dataSource.count>0) {
        [self.tableView reloadData];
        NSLog(@"%@", NSStringFromCGRect(self.tableView.frame));
    }
}


// 清空
- (void)resetSelected
{
    for (KLSelectTableRow *row in self.tableDataSource) {
        if ([row isMenu]) {
            for (KLSelectTableRow *childRow in [row getAllChildRows]) {
                childRow.isSelected = NO;
            }
        }
        row.isSelected = NO;
    }
    [self.tableView reloadData];
}


- (NSDictionary *)selectedResult
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    for (KLSelectTableRow *row in self.dataSource) {
        [result addEntriesFromDictionary:[row getAllSelectedLeafRowValues]];
    }
    return result;
}

- (NSArray *)selectedTopmostRows
{
    NSMutableArray *mArr = [NSMutableArray array];
    for (KLSelectTableRow *row in self.dataSource) {
        [mArr addObjectsFromArray:[row getSelectedTopmostRows]];
    }
    return mArr;
}

- (BOOL)hasSelected
{
    NSDictionary *selectedResult = [self selectedResult];
    BOOL hasSelected = NO;
    if (selectedResult.allKeys.count>0) {
        for (NSString *key in selectedResult.allKeys) {
            NSArray *rowArray = [selectedResult valueForKey:key];
            if (rowArray.count > 0) {
                hasSelected = YES;
                break;
            }
        }
    }
    return hasSelected;
}

- (NSMutableArray<KLSelectTableRow *> *)tableDataSource
{
    if (!_tableDataSource) {
        _tableDataSource = [NSMutableArray array];
    }
    return _tableDataSource;
}

- (void)setDataSource:(NSArray<KLSelectTableRow *> *)dataSource
{
    _dataSource = dataSource;
    [self.tableDataSource removeAllObjects];
    for (KLSelectTableRow *row in dataSource) {
        [self.tableDataSource addObject:row];
        if ([row isMenu]) {
            if (row.disable && row.isUnfold) {
                row.isUnfold = NO;
            }
            [self.tableDataSource addObjectsFromArray:[row getChildRows]];
        }
    }
    [self.tableView reloadData];
}

- (void)setTableViewBackGroundColor:(UIColor *)color {
    self.tableView.backgroundColor = color;
}

- (UIView *)filterView {
    if (!_filterView) {
        _filterView = [[UIView alloc] initWithFrame:CGRectZero];
        _filterView.translatesAutoresizingMaskIntoConstraints = NO;
        [_filterView addSubview:self.filterBGView];

        NSLayoutConstraint *viewTop = [NSLayoutConstraint constraintWithItem:self.filterBGView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_filterView attribute:NSLayoutAttributeTop multiplier:1 constant:8];
        NSLayoutConstraint *viewLeft = [NSLayoutConstraint constraintWithItem:self.filterBGView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:_filterView attribute:NSLayoutAttributeLeft multiplier:1 constant:16];
        NSLayoutConstraint *viewBottom = [NSLayoutConstraint constraintWithItem:self.filterBGView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_filterView attribute:NSLayoutAttributeBottom multiplier:1 constant:-8];
        NSLayoutConstraint *viewRight = [NSLayoutConstraint constraintWithItem:self.filterBGView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:_filterView attribute:NSLayoutAttributeRight multiplier:1 constant:-16];

        [_filterView addConstraint:viewTop];
        [_filterView addConstraint:viewLeft];
        [_filterView addConstraint:viewBottom];
        [_filterView addConstraint:viewRight];
    }
    return _filterView;
}

- (UIView *)filterBGView {
    if (!_filterBGView) {
        _filterBGView = [UIView new];
        _filterBGView.translatesAutoresizingMaskIntoConstraints = NO;
        _filterBGView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0];
        _filterBGView.layer.cornerRadius = 4.f;
        _filterView.layer.masksToBounds = YES;
        
        [_filterBGView addSubview:self.filterIcon];
        [_filterBGView addSubview:self.filterTextField];

        NSLayoutConstraint *filterIconCenterY = [NSLayoutConstraint constraintWithItem:self.filterIcon attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_filterBGView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
        NSLayoutConstraint *filterIconLeft = [NSLayoutConstraint constraintWithItem:self.filterIcon attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:_filterBGView attribute:NSLayoutAttributeLeft multiplier:1 constant:16];
        NSLayoutConstraint *filterIconWidth = [NSLayoutConstraint constraintWithItem:self.filterIcon attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:24];
        NSLayoutConstraint *filterIconHeight = [NSLayoutConstraint constraintWithItem:self.filterIcon attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:24];

        [_filterBGView addConstraint:filterIconCenterY];
        [_filterBGView addConstraint:filterIconLeft];
        [_filterBGView addConstraint:filterIconWidth];
        [_filterBGView addConstraint:filterIconHeight];

        NSLayoutConstraint *filterTextFieldTop = [NSLayoutConstraint constraintWithItem:self.filterTextField attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_filterBGView attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        NSLayoutConstraint *filterTextFieldLeft = [NSLayoutConstraint constraintWithItem:self.filterTextField attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.filterIcon attribute:NSLayoutAttributeRight multiplier:1 constant:10];
        NSLayoutConstraint *filterTextFieldBottom = [NSLayoutConstraint constraintWithItem:self.filterTextField attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_filterBGView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        NSLayoutConstraint *filterTextFieldRight = [NSLayoutConstraint constraintWithItem:self.filterTextField attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:_filterBGView attribute:NSLayoutAttributeRight multiplier:1 constant:0];

        [_filterBGView addConstraint:filterTextFieldTop];
        [_filterBGView addConstraint:filterTextFieldLeft];
        [_filterBGView addConstraint:filterTextFieldBottom];
        [_filterBGView addConstraint:filterTextFieldRight];
        
    }
    return _filterBGView;
}

- (UIImageView *)filterIcon {
    if (!_filterIcon) {
        _filterIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconSelectTableSearch.png"]];
        _filterIcon.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _filterIcon;
}

- (UITextField *)filterTextField {
    if (!_filterTextField) {
        _filterTextField = [[UITextField alloc] init];
        _filterTextField.translatesAutoresizingMaskIntoConstraints = NO;
        _filterTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _filterTextField.placeholder = @"Search";
        [_filterTextField addTarget:self action:@selector(changeTextField:) forControlEvents:UIControlEventEditingChanged];
    }
    return _filterTextField;
}

- (void)changeTextField:(UITextField *)textField {
    NSString *filterString = textField.text;
    if (textField.text.length == 0) {
        self.isFilter = NO;
    } else {
        self.isFilter = YES;
    }
    
    if (self.isFilter) {
        [self.filterDataSource removeAllObjects];
        
        for (KLSelectTableRow *row in self.rows) {
            if (row.disable) {
                continue;
            }
            
            if ([row isMenu]) {
                BOOL isContainChildRow = NO;
                NSArray<KLSelectTableRow *> *rows = [row getAllChildRows];
                for (KLSelectTableRow *_row in rows) {
                    if ([_row.title containsString:filterString]) {
                        
                        NSArray<KLSelectTableRow *> *_pRows = [_row getParentRows];
                        for (KLSelectTableRow *prow in _pRows) {
                            if (![self.filterDataSource containsObject:prow]) {
                                [self.filterDataSource addObject:prow];
                            }
                        }
                        
                        [self.filterDataSource addObject:_row];
                        isContainChildRow = YES;
                    }
                }
                if (!isContainChildRow && [row.title containsString:filterString]) {
                    [self.filterDataSource addObject:row];
                }
            } else {
                if ([row.title containsString:filterString]) {
                    [self.filterDataSource addObject:row];
                }
            }
        }
        
        
        
        self.tableDataSource = self.filterDataSource;
    } else {
        self.dataSource = self.rows;
    }
    
    [self.tableView reloadData];
}

- (NSMutableArray<KLSelectTableRow *> *)filterDataSource {
    if (!_filterDataSource) {
        _filterDataSource = [NSMutableArray<KLSelectTableRow *> array];
    }
    return _filterDataSource;
}



@end


